let users = []

loadUsers = () => {
  return fetch('https://randomuser.me/api/?results=100')
    .then(result => result.json())
    .then(data => {
      users = data.results
      displayUsers(users)
    })
}


displayUsers = (users) => {
  document.querySelector('#users').innerHTML = users.map(user => 
    `<li class="list-group-item"> 
        <img class="img-responsive pull-right" align="center"style="max-height:30px; border-radius: 50%" src="${user.picture.thumbnail}"/>
        ${user.name.first} ${user.name.last}
        ${user.email}
        ${user.dob.age}
        ${user.gender}
        ${user.location.street.name} ${user.location.street.number},
        ${user.location.city}, ${user.location.state}

    </li>`).join('')
}

loadUsers()